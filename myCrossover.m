function xoverKids  = crossoverarithmetic(parents,options,GenomeLength,FitnessFcn,unused,thisPopulation)

% How many children to produce?
nKids = length(parents);
% Allocate space for the kids
xoverKids = zeros(nKids,GenomeLength);
% To move through the parents twice as fast as the kids are
% being produced, a separate index for the parents is needed
index = 1;
% for each kid...
for i=1:nKids
    % get parents(choose parents randomly from the selection of 25 parents)
    rp = randperm(25,2);
    r1 = rp(1);
    r2 = rp(2);
    
    % Children are arithmetic mean of two parents
    alpha = 0.5;
    xoverKids(i,:) = alpha*thisPopulation(r1,:) + alpha*thisPopulation(r2,:);
end
