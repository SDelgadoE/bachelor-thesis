function [SS, OLS] = myfitnessfcn(x, y, t)
ft = (x(1)-t).^x(2);
gt = ((x(1)-t).^x(2)).*cos(x(3).*log(x(1)-t)+x(4));
K = [(ones(length(ft),1)) ft gt];

OLS = K\y;

SS = sum((y - OLS(1) - OLS(2).*((x(1)-t).^x(2)) - OLS(3).*((x(1)-t).^x(2)).*cos(x(3).*log(x(1)-t)+x(4))).^2);

end