 function [SS, RSquared, A, B, C, tc, z, w, phi, yfit] = myGA(Start, End, SP500)
warning('off','MATLAB:rankDeficientMatrix');
%% Load nessecary data
load('SP500.mat');
%SP500.prices = log(SP500.prices);
%% Create a new subperiod of the time series
LS = find(SP500.dates==Start);
LE = find(SP500.dates==End);
t = zeros;
y = zeros;

t(1:(LE-LS+1),1) = SP500.dates(LS:LE);
y(1:(LE-LS+1),1) = SP500.prices(LS:LE);
%% GA Bounds
% x(1) = tc,   x(2) = z,   x(3) = w,   x(4) = phi,   x(5) = A,   x(6) = B,   x(7) = C

LB = [t(end); 0; 5; -2*pi];   % Lower bound
UB = [t(end)+1200; 1; 15; 2*pi];  % Upper bound
%% Set GA Options
options = gaoptimset;
% Modify options setting
options = gaoptimset(options,'EliteCount',50,'SelectionFcn', @parents,...,
    'PopulationSize', 100,'CrossoverFraction', 0.25,'CrossoverFcn',@myCrossover...
    ,'Generations',100,'TolFun',0.001,'FitnessScalingFcn',@fitscalingrank ...
    ,'MigrationFraction',0,'Display','off','MutationFcn',{@myMutationFcn, 1});

dbstop if error; 
[x] = ...
ga(@(x)myfitnessfcn(x,y,t),4,[],[],[],[],LB,UB,[],[],options);%,fval,exitflag,output,population,score
%% Get OLS estimates
[SS, OLS] = myfitnessfcn(x, y, t);
%% Create an alternate t and y so we can show the full value of yfit
ND = floor(x(1))-t(end);
NewEnd = End + ND;
[~,I] = min(abs(SP500.dates-NewEnd));
NewEnd2 = SP500.dates(I);

LS = find(SP500.dates==Start);
LE = find(SP500.dates==NewEnd2);

t2 = zeros;
y2 = zeros;

t2(1:(LE-LS+1),1) = SP500.dates(LS:LE);
y2(1:(LE-LS+1),1) = SP500.prices(LS:LE);
%% Calculate ft and gt
% These are the results, no longer non-linear
ft = (x(1)-t2).^x(2);
gt = ((x(1)-t2).^x(2)).*cos(x(3).*log(x(1)-t2)+x(4));
%% Construct the fitted values with the nex OLS results for ABC and the
% previous values of ft and gt
yfit = OLS(1) + OLS(2).*ft + OLS(3).*gt; 
% delete complex values in yfit and correct t2 accordingly
d = find(imag(yfit(:,1)),1);
yfit(d:end,:) = [];
t2(d:end,:) = [];
y2(d:end,:) = [];
%% Plot the fit against the prices
%figure
%plot(t,y)
%datetick( 'x', 'dd-mm-yyyy' )
%hold on
%plot(t2, yfit)
%title('GA1')
%% Compute R Squared
Ybar = mean(y2);
TotalSS = sum((y2-Ybar).^2);
ResidualSS = sum((y2-yfit).^2);
A =OLS(1);
B = OLS(2);
C = OLS(3);
tc = x(1);
z = x(2);
w = x(3);
phi = x(4);

%DatePredicted = datestr(x(1));
RSquared = 1-(ResidualSS/TotalSS);
%% Levenber-Marquardt nonlinear least squares algorithm
lb = [-Inf; -Inf; -Inf; -Inf];   % Lower bound
ub = [Inf; Inf; Inf; Inf];  % Upper bound
%xold = x;
% use the same dates and prices as the ga search
%t = t;
%y = y;
options = optimoptions(@lsqnonlin, 'TolFun',1e-8, 'Algorithm','levenberg-marquardt','Diagnostics','off','FinDiffType','central','display','off');
[LMx] = lsqnonlin(@(x)myfitnessfcn(x, y ,t),[x(1),x(2), x(3), x(4)],lb,ub,options);
x = LMx;
[SSLMx, OLS] = myfitnessfcn(x, y, t);
% check to see if it found a better soultion and if so calculate all the
% new figures

if SSLMx<SS
    %fprintf('lsqnonlin found a better solution')
    if LMx(1)>t(end) % correct dates and prices
        ND = floor(x(1))-t(end);
        NewEnd = End + ND;
        [~,I] = min(abs(SP500.dates-NewEnd));
        NewEnd2 = SP500.dates(I);
        
        LS = find(SP500.dates==Start);
        LE = find(SP500.dates==NewEnd2);
        
        t2 = zeros;
        y2 = zeros;
        
        t2(1:(LE-LS+1),1) = SP500.dates(LS:LE);
        y2(1:(LE-LS+1),1) = SP500.prices(LS:LE);
       
    elseif LMx(1)<t(end)
        t2 = t;
        y2 = y;
    end
    
    ft = (x(1)-t2).^x(2);
    gt = ((x(1)-t2).^x(2)).*cos(x(3).*log(x(1)-t2)+x(4));
    yfit = OLS(1) + OLS(2).*ft + OLS(3).*gt;
    d = find(imag(yfit(:,1)),1);
    yfit(d:end,:) = [];
    t2(d:end,:) = [];
    y2(d:end,:) = [];
  
    %figure;
    %plot(t2,y2);
    %datetick( 'x', 'dd-mm-yyyy' );
    %hold on;
    %plot(t2, yfit);
    %title('Taboo2');
    
    
    %DatePredicted = datestr(x(1));
    Ybar = mean(y2);
    TotalSS = sum((y2-Ybar).^2);
    ResidualSS = sum((y2-yfit).^2);
    RSquared = 1-(ResidualSS/TotalSS);
    A =OLS(1);
    B = OLS(2);
    C = OLS(3);
    tc = x(1);
    phi = x(4);
    w = x(3);
    z = x(2);
    SS = SSLMx;
elseif SSLMx>SS;
    %fprintf('lsqnonlin did not find a better solution')
elseif SSLMx==SS;
    %fprintf('lsqnonlin found the same solution')
end
 end
