clc
clear
load('NQ.mat');
rng('shuffle')
%% preallocate
GA2cell= cell(1,6);
GAfitcell = cell(1,6);
Taboo2cell = cell(1,6);
Taboofitcell = cell(1,6);
%% Run the program over 3 different periods 2 years apart
for o = 1:6
    %% Epanding time window
    Win2 = datenum('10.03.2000','dd.mm.yyyy');
    % number of estimations(every twentyfive trading days)
    
    % find the closest available trading day for our chosen dates
    [c, index] = min(abs(NQ.dates-Win2));
    Win2 = NQ.dates(index);
    
    xx = find(NQ.dates==Win2) - 200 - 50*o;
    yy = find(NQ.dates==Win2);
    Win1 = NQ.dates(xx);
    
    NumIterations = (round((yy-xx)/10)-20);
    
    Starting = datestr(Win1);
    Ending = datestr(NQ.dates((xx+200+NumIterations*10)));
    
    if ismember(Win1, NQ.dates);
        
        GA2 = zeros(NumIterations,13);
        Taboo2 = zeros(NumIterations,13);
        GAfit  = cell(1,NumIterations);
        Taboofit = cell(1,NumIterations);
        %% Call the GA and Taboo functions
        for h = 1:NumIterations
            Start = Win1;
            End = NQ.dates((xx+200+h*10));
            disp(['Period ' num2str(o) ' of 6'])            
            disp(['Suberiod ' num2str(h) ' of ' num2str(NumIterations)])
            
            % Store results: t1,t2,SS,Rsquared,A,B,C,tc,z,w,phi.
            AS = zeros(10,13);
            ASfit = cell(1,10);
            for iGA = 1:10
                %display(iGA)
                [SS, RSquared, A, B, C, tc, phi, w, z, yfit] = myGANQ(Start, End);
                AS(iGA,1) = Start;
                AS(iGA,2) = End;
                AS(iGA,3)= SS;
                AS(iGA,4) = RSquared;
                AS(iGA,5) = A;
                AS(iGA,6) = B;
                AS(iGA,7) = C;
                AS(iGA,8) = tc;
                AS(iGA,9) = z;
                AS(iGA,10) = w;
                AS(iGA,11) = phi;
                AS(iGA,12) = h;
                AS(iGA,13) = iGA;
                ASfit{iGA} = yfit;
            end
             % delete useless
            AS = sortrows(AS,3);
            for d = 1:9;
                if nnz(imag(AS(:,3))) < 10;
                    if imag(AS(1,3)) ~= 0
                    AS(1,:) = [];
                    end
                end
            end
            % select best solution out of the ten and save it
            GA2(h,:) = AS(1,:);
            GAfit{h} = ASfit{GA2(h,13)};
            
            % Store results: t1,t2,SS,Rsquared,A,B,C,tc,z,w,phi.
            AS = zeros(10,13);
            ASfit = cell(1,10);
            for iTS = 1:10
                %display(iTS)
                [SS, RSquared, A, B, C, tc, phi, w, z, yfit] = myTABOONQ(Start, End);
                AS(iTS,1) = Start;
                AS(iTS,2) = End;
                AS(iTS,3)= SS;
                AS(iTS,4) = RSquared;
                AS(iTS,5) = A;
                AS(iTS,6) = B;
                AS(iTS,7) = C;
                AS(iTS,8) = tc;
                AS(iTS,9) = z;
                AS(iTS,10) = w;
                AS(iTS,11) = phi;
                AS(iTS,12) = h;
                AS(iTS,13) = iTS;
                ASfit{iTS} = yfit;
            end
             % delete useless    
            AS = sortrows(AS,3);
            for d = 1:9;
                if nnz(imag(AS(:,3))) < 10;
                    if imag(AS(1,3)) ~= 0
                    AS(1,:) = [];
                    end
                end
            end
            % select best solution out of the ten and save it
            Taboo2(h,:) = AS(1,:);
            Taboofit{h} = ASfit{Taboo2(h,13)};
            
        end
    end
    
    %% Create crash lock-in plots
    figure(o)
    subplot(1,2,1);
    scatter(GA2(:,2),GA2(:,8),'filled')
    axis square
    axis([datenum('01011998','ddmmyyyy'),datenum('01012002','ddmmyyyy'),datenum('01011998','ddmmyyyy'),datenum('01012002','ddmmyyyy')])
    datetick( 'x', 'yyyy' )
    datetick( 'y', 'yyyy')
    title('Genetic Algorithm')
    xlabel('Last observation of t')
    ylabel('Estimated crash cate t_c')
    line([NQ.dates(1) NQ.dates(end)], [mean(GA2(:,8)) mean(GA2(:,8))],'Color','r','LineWidth',1)
    legend('Predicted crash dates','Mean of t_c','location','southeast')
    
    
    subplot(1,2,2);
    scatter(Taboo2(:,2),Taboo2(:,8),'filled')
    axis square
    axis([datenum('01011998','ddmmyyyy'),datenum('01012002','ddmmyyyy'),datenum('01011998','ddmmyyyy'),datenum('01012002','ddmmyyyy')])
    datetick( 'x', 'yyyy' )
    datetick( 'y', 'yyyy')
    title('Taboo Search')
    xlabel('Last observation of t')
    line([NQ.dates(1) NQ.dates(end)], [mean(Taboo2(:,8)) mean(Taboo2(:,8))],'Color','r','LineWidth',1)
    
    %[ax,h1] = suplabel('Date of the last observation in the estimation sample ');
    
    
    filename = [ 'PLC.00_' num2str(o) '.tikz' ];
    
    matlab2tikz(filename, 'height', '\figureheight', 'width', '\figurewidth','showInfo', false)
       
    %% Save to cell
    
    GA2cell{o} = GA2;
    GAfitcell{o} = GAfit;
    Taboo2cell{o} = Taboo2;
    Taboofitcell{o} = Taboofit;
end
save ('RR00.mat', 'GA2cell','Taboo2cell','GAfitcell','Taboofitcell')
