clc
clear
%% Import the relevant data
load('sigmazero.mat');
SigmaZero = Extrema2;
Extrema2 = [];

load('sigma.mat');
Sigma = Extrema2;
Extrema2 = [];

load('sigma3.mat');
Sigma3 = Extrema2;
Extrema2 = [];

%% Calculate drawdowns/ups as percentages for Epsilon = 0 (sigma zero)
PercentageDeltasSZ = zeros;
for n = 1:(length(SigmaZero)-1)   
PercentageDeltasSZ(n+1,1) = (SigmaZero(n+1,2)-SigmaZero(n,2))/SigmaZero(n,2);
end
% Find drawdowns
DrawdownsSZ = PercentageDeltasSZ;
DrawdownsSZ(DrawdownsSZ >= 0) = 0;
DrawdownsSZ(DrawdownsSZ == 0) = [];
% Arrange from biggest to smallest
DrawdownsSZ = sort(DrawdownsSZ);
DrawdownsSZ(:,2) = 1:length(DrawdownsSZ);
%% Regress the drawdowns on a exponential distribution
NSZ(:,1) = (DrawdownsSZ(:,2));
DSZ(:,1) = (DrawdownsSZ(:,1));

logNSZ = log(NSZ);

%% Calculate drawdowns/ups as percentages for Epsilon = sigma
PercentageDeltasS = zeros;
for n = 1:(length(Sigma)-1)   
PercentageDeltasS(n+1,1) = (Sigma(n+1,2)-Sigma(n,2))/Sigma(n,2);
end
% Find drawdowns
DrawdownsS = PercentageDeltasS;
DrawdownsS(DrawdownsS >= 0) = 0;
DrawdownsS(DrawdownsS == 0) = [];
% Arrange from biggest to smallest
DrawdownsS = sort(DrawdownsS);
DrawdownsS(:,2) = 1:length(DrawdownsS);
%% Regress the drawdowns on a exponential distribution
NS(:,1) = (DrawdownsS(:,2));
DS(:,1) = (DrawdownsS(:,1));

logNS = log(NS);

%% Calculate drawdowns/ups as percentages for Epsilon = 3*sigma
PercentageDeltasS3 = zeros;
for n = 1:(length(Sigma3)-1)   
PercentageDeltasS3(n+1,1) = (Sigma3(n+1,2)-Sigma3(n,2))/Sigma3(n,2);
end
% Find drawdowns
DrawdownsS3 = PercentageDeltasS3;
DrawdownsS3(DrawdownsS3 >= 0) = 0;
DrawdownsS3(DrawdownsS3 == 0) = [];
% Arrange from biggest to smallest
DrawdownsS3 = sort(DrawdownsS3);
DrawdownsS3(:,2) = 1:length(DrawdownsS3);
% Regress the drawdowns on a exponential distribution
NS3(:,1) = (DrawdownsS3(:,2));
DS3(:,1) = (DrawdownsS3(:,1));

logNS3 = log(NS3);

%% Create a figure
figure(1)
scatter(DrawdownsSZ(:,1), logNSZ)
hold on 
scatter(DrawdownsS(:,1), logNS)
hold on
scatter(DrawdownsS3(:,1), logNS3)

%% Use cftool to see the fits
%cftool
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on')
hold on
h1=plot(sigma3fit,':r')

h2=plot(DS3,logNS3, 'or' )


h3=plot(sigmafit,'-.g')

h4=plot(DS,logNS, '+g')

h5=plot(sigmazerofit,'-b')

h6=plot(DSZ,logNSZ,'db')
xlim([-0.3 0])

legend([h1,h2,h3,h4,h5,h6])
% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.137570031029579 0.684442640692639 0.213287391902515 0.212121212121211],...
    'FontSize',9);

[fitresult, gof] = createFits(DS3, logNS3, DSZ, logNSZ, DS, logNS)
%% Create table of results for latex 
epsigma3 = coeffvalues(sigma3fit)
epsigma = coeffvalues(sigmafit)
epsigmazero = coeffvalues(sigmazerofit) 

T1 = [epsigmazero;epsigma;epsigma3]
latex_T1 = latex(sym(T1));
%% Find the biggest 5 drawdowns for each epsilon

BD = DSZ(1:5,:);

PercentageDeltasSZ(:,2) = SigmaZero(:,3); 
LocPD =zeros;
LocPD = find(ismember(PercentageDeltasSZ(:,1),[BD]));

for a = LocPD(:,1)    
LocPD(:,2) = PercentageDeltasSZ(a,2);
LocPD(:,3) = PercentageDeltasSZ(a,1);
end
B = sortrows(LocPD,3)
% LocPD now has the dates 

Dates = datestr(B(:,2),'dd.mmm.yyyy');



save ('datessigma3.mat', 'Dates')
save ('LocPDsigma3.mat', 'LocPD')

