function mutationChildren = myMutationFcn(parents,options,GenomeLength,FitnessFcn,state,thisScore,thisPopulation,mutationRate)


if nargin < 8 || isempty(mutationRate)
    mutationRate = 1; % default mutation rate
end

if(strcmpi(options.PopulationType,'doubleVector'))
    mutationChildren = zeros(25,GenomeLength);%length(parents),GenomeLength);
    for i=1:25 % length(parents)
        %select 25 randomly without replacement to be mutated
        child = thisPopulation(randsample(50,25),:);
        % each gene is replaced with a value chosen randomly from the range.
        for v = 1:4
            R (:,v) = (range(state.Population(:,v)))*((4)*rand(1)-2);
        end
        % range can have one column or one for each gene.
        mutationChildren(i,:) = (child(i,:) + R);
    end
    
end
end
