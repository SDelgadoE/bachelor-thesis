clc
clear
%% Load data


load('RR00.mat');
CrashDateNQ = datenum('10.03.2000','dd.mm.yyyy');
GA2cellNQ = GA2cell;
Taboo2cellNQ = Taboo2cell;

%% Compare results with filtering NASDAQ
for o = 1:6
%% Filter solution
GA2 = cell2mat(GA2cellNQ(o));
Taboo2 = cell2mat(Taboo2cellNQ(o));
   
 % 0<z<1
    D = find(Taboo2(:,9)<0);
    Taboo2(D,:) = [];
    D = find(Taboo2(:,9)>1);
    Taboo2(D,:) = [];
    D = find(GA2(:,9)<0);
    GA2(D,:) = [];
    D = find(GA2(:,9)>1);
    GA2(D,:) = [];
    
    % B<0
    D = find(Taboo2(:,6)>0);
    Taboo2(D,:) = [];
    D = find(GA2(:,6)>0);
    GA2(D,:) = [];
    
    % if = NaN
    D = find(isnan(Taboo2(:,4)));
    Taboo2(D,:) = [];
    D = find(isnan(GA2(:,4)));
    GA2(D,:) = [];
    
    % t_c>Win1
    D = find(Taboo2(:,8)<Taboo2(:,1));
    Taboo2(D,:) = [];
    D = find(GA2(:,8)<GA2(:,1));
    GA2(D,:) = [];
    % R^2 <0
    D = find(Taboo2(:,4)<0);
    Taboo2(D,:) = [];
    D = find(GA2(:,4)<0);
    GA2(D,:) = [];

% make matrix containing all the prediction accuracies of each period
PAnq{o,1} = (abs(CrashDateNQ - GA2(:,8)));
PAnq{o,2} = (abs(CrashDateNQ - Taboo2(:,8)));

RSSnq{o,1} = (GA2(:,3));
RSSnq{o,2} = (Taboo2(:,3));

R2nq{o,1} = GA2(:,4);
R2nq{o,2} = Taboo2(:,4);

end
%% final averages for the periods
%  iterate over the 6 periods
for o =1:6
PA(o,1) = mean(cell2mat(PAnq(o,1)))
PA(o,2) = mean(cell2mat(PAnq(o,2)))

RSS(o,1) = mean(cell2mat(RSSnq(o,1)))
RSS(o,2) = mean(cell2mat(RSSnq(o,2)))

R2(o,1) = mean(cell2mat(R2nq(o,1)))
R2(o,2) = mean(cell2mat(R2nq(o,2)))
end
%% final averages for the algos
PAga = mean(cell2mat(PAnq(:,1)))
PAts = mean(cell2mat(PAnq(:,2)))

RSSga = mean(cell2mat(RSSnq(:,1)))
RSSts = mean(cell2mat(RSSnq(:,2)))

R2ga = mean(cell2mat(R2nq(:,1)))
R2ts = mean(cell2mat(R2nq(:,2)))