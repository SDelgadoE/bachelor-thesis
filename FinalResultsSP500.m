clc
clear
%% Load data

load('RR87.mat');
CrashDateSP500 = datenum('23.09.1987','dd.mm.yyyy');
GA2cellSP500 = GA2cell;
Taboo2cellSP500 = Taboo2cell;
GA2cell = [];
Taboo2cell = [];

%% Compare results with filtering SP500
%prediction accuracy average of each estimation sample
%PAsp500 =zeros;

for o = 1:6
%% Filter solution
GA2 = cell2mat(GA2cellSP500(o));
Taboo2 = cell2mat(Taboo2cellSP500(o));
   
 % 0<z<1
    D = find(Taboo2(:,9)<0);
    Taboo2(D,:) = [];
    D = find(Taboo2(:,9)>1);
    Taboo2(D,:) = [];
    D = find(GA2(:,9)<0);
    GA2(D,:) = [];
    D = find(GA2(:,9)>1);
    GA2(D,:) = [];
    
    % B<0
    D = find(Taboo2(:,6)>0);
    Taboo2(D,:) = [];
    D = find(GA2(:,6)>0);
    GA2(D,:) = [];
    
    % if = NaN
    D = find(isnan(Taboo2(:,4)));
    Taboo2(D,:) = [];
    D = find(isnan(GA2(:,4)));
    GA2(D,:) = [];
    
    % t_c>Win1
    D = find(Taboo2(:,8)<Taboo2(:,1));
    Taboo2(D,:) = [];
    D = find(GA2(:,8)<GA2(:,1));
    GA2(D,:) = [];
    % R^2 <0
    D = find(Taboo2(:,4)<0);
    Taboo2(D,:) = [];
    D = find(GA2(:,4)<0);
    GA2(D,:) = [];

% make matrix containing all the prediction accuracies of each period
PAsp500{o,1} = (abs(CrashDateSP500 - GA2(:,8)));
PAsp500{o,2} = (abs(CrashDateSP500 - Taboo2(:,8)));

RSSsp500{o,1} = GA2(:,3);
RSSsp500{o,2} = Taboo2(:,3);

R2sp500{o,1} = GA2(:,4);
R2sp500{o,2} = Taboo2(:,4);

end
%% final averages for the periods
%  iterate over the 6 periods
for o =1:6
PA(o,1) = mean(cell2mat(PAsp500(o,1)));
PA(o,2) = mean(cell2mat(PAsp500(o,2)));

RSS(o,1) = mean(cell2mat(RSSsp500(o,1)));
RSS(o,2) = mean(cell2mat(RSSsp500(o,2)))

R2(o,1) = mean(cell2mat(R2sp500(o,1)));
R2(o,2) = mean(cell2mat(R2sp500(o,2)));
end
%% final averages for the algos
PAga = mean(cell2mat(PAsp500(:,1)))
PAts = mean(cell2mat(PAsp500(:,2)))

RSSga = mean(cell2mat(RSSsp500(:,1)))
RSSts = mean(cell2mat(RSSsp500(:,2)))

R2ga = mean(cell2mat(R2sp500(:,1)))
R2ts = mean(cell2mat(R2sp500(:,2)))






