%% Plot results
clc
clear
load('NQ.mat');
%NQ.prices = log(NQ.prices);
load('RR00.mat');
CrashDate = datenum('10.03.2000','dd.mm.yyyy');

format short

%% Load a big interval
Start = datenum('01.01.1998','dd.mm.yyyy');
[c, index] = min(abs(NQ.dates-Start));
Start = NQ.dates(index);
End = datenum('31.06.2000','dd.mm.yyyy');
[c, index] = min(abs(NQ.dates-End));
End = NQ.dates(index);

LS = find(NQ.dates==Start);
LE = find(NQ.dates==End);
tt = zeros;
yy = zeros;

tt(1:(LE-LS+1),1) = NQ.dates(LS:LE);
yy(1:(LE-LS+1),1) = NQ.prices(LS:LE);
%% Start loop to go over the 4 solutions
for o = 1:6
    %% load solutions
    GA2 = cell2mat(GA2cell(o));
    Taboo2 = cell2mat(Taboo2cell(o));
    
    
    
    figure(o)
  
    if isempty(GA2)==0   
    
    %% Choose solution to plot GA2
    %[c, S] = min(abs(CrashDate-GA2(:,8)));
    S = find(GA2(:,4)==max(GA2(:,4)));
    BS = GA2(S,:);
    
    x(1)=BS(8);
    x(2)=BS(9);
    x(3)=BS(10);
    x(4)=BS(11);
    
    OLS(1)=BS(5);
    OLS(2)=BS(6);
    OLS(3)=BS(7);
    
    tc = x(1);
    tcA = abs(CrashDate - x(1));    
    %% Load a the time interval used to find our solution
    Start = BS(1);
    [c, index] = min(abs(NQ.dates-Start));
    Start = NQ.dates(index);
    End = BS(2);
    [c, index] = min(abs(NQ.dates-End));
    End = NQ.dates(index);
    
    LS = find(NQ.dates==Start);
    LE = find(NQ.dates==End);
    t = zeros;
    y = zeros;
    
    t(1:(LE-LS+1),1) = NQ.dates(LS:LE);
    y(1:(LE-LS+1),1) = NQ.prices(LS:LE);
    %% New Interval
    
    [~,I] = min(abs(NQ.dates-x(1)));
    NewEnd2 = NQ.dates(I);
    
    LS = find(NQ.dates==BS(1));
    LE = find(NQ.dates==NewEnd2);
    
    t2 = zeros;
    y2 = zeros;
    t2(1:(LE-LS+1),1) = NQ.dates(LS:LE);
    y2(1:(LE-LS+1),1) = NQ.prices(LS:LE);
    
    yfit = (GAfitcell{1,o}{BS(12)});
    
    d = find(imag(yfit(:,1)),1);
    yfit(d:end,:) = [];
    t2(d:end,:) = [];
    t2(length(yfit)+1:end,:) = [];
    %% Plot the fit against the prices
    
    subplot(2,1,1);
    
    plot(tt,yy,'k:');
    
    axis([(tt(1)),(tt(end)),(min(yy)-10),(max(yy)+10)]);
    hold on;
    plot(t,y,'k');
    plot(t2, yfit,'r');
    xlabel('t');
    ylabel('p(t)');
    box off;
    
    h = gca;
    h.XTickMode = 'manual';
    h.XTick=[datenum('01011994','ddmmyyyy'), datenum('01011995','ddmmyyyy'), datenum('01011996','ddmmyyyy'),...
        datenum('0101997','ddmmyyyy'), datenum('01011998','ddmmyyyy'), datenum('01011999','ddmmyyyy'),...
        datenum('01012000','ddmmyyyy'),  datenum('01012001','ddmmyyyy')];
    h.Title.String = ('Genetic Algorithm');
    h.YLabel.String = 'p(t)';
    h.XLabel.String = 't';
    line([tc tc], [0 10000],'Color','b','LineWidth',1);
    datetick( 'x', 'yyyy','keeplimits');
    
    str = {['A = '  num2str(round(BS(5),3),'%.3f')]...
        ['B = ', num2str(round(BS(6),3),'%.3f')] ...
        ['C = ', num2str(round(BS(7),3),'%.3f')]...
        ['t_{c} = ', datestr(BS(8),'dd.mm.yy')] ...
        ['z = ', num2str(round(BS(9),3),'%.3f')] ...
        ['\omega = ' num2str(round(BS(10),3),'%.3f')]...
        ['\Phi = ', num2str(round(BS(11),3),'%.3f')]...
        ['R^{2} = ', num2str(round(BS(4),3),'%.3f')]};
    
    annotation('textbox',[0.15, 0.925, 0.1,0.1], ...
        'String', str,'interpreter','tex', 'FitBoxToText', 'on', 'BackgroundColor', 'w', 'Margin',6);
    %text(0.02,0.98,'(a)','Units', 'Normalized', 'VerticalAlignment', 'Top')
    %legend('SP&500 continuation','estimated time series S&P 500', 'fitted values','predicted crash date', 'Location','northhwest');
    %
 end


%% Choose solution to plot Taboo2
S = find(Taboo2(:,4)==max(Taboo2(:,4)));
%[c, S] = min(abs(CrashDate-Taboo2(:,8)))
BS = Taboo2(S,:);

x(1)=BS(8);
x(2)=BS(9);
x(3)=BS(10);
x(4)=BS(11);

OLS(1)=BS(5);
OLS(2)=BS(6);
OLS(3)=BS(7);

tc = x(1);
tcA = abs(CrashDate - x(1));
%% Load a the time interval used to find our solution
Start = BS(1);
[c, index] = min(abs(NQ.dates-Start));
Start = NQ.dates(index);
End = BS(2);
[c, index] = min(abs(NQ.dates-End));
End = NQ.dates(index);

LS = find(NQ.dates==Start);
LE = find(NQ.dates==End);
t = zeros;
y = zeros;

t(1:(LE-LS+1),1) = NQ.dates(LS:LE);
y(1:(LE-LS+1),1) = NQ.prices(LS:LE);
%% New Interval
[~,I] = min(abs(NQ.dates-x(1)));
NewEnd2 = NQ.dates(I);

LS = find(NQ.dates==BS(1));
LE = find(NQ.dates==NewEnd2);

t2 = zeros;
y2 = zeros;
t2(1:(LE-LS+1),1) = NQ.dates(LS:LE);
y2(1:(LE-LS+1),1) = NQ.prices(LS:LE);

yfit = (Taboofitcell{1,o}{BS(12)});

d = find(imag(yfit(:,1)),1);
yfit(d:end,:) = [];
t2(d:end,:) = [];
t2(length(yfit)+1:end,:) = [];
%% Plot the fit against the prices

subplot(2,1,2);
plot(tt,yy,'k:');
axis([(tt(1)),(tt(end)),(min(yy)-10),(max(yy)+10)])
hold on;
plot(t,y,'k');
plot(t2, yfit,'r')
xlabel('t')
ylabel('p(t)')
box off

h = gca;
h.XTickMode = 'manual'
h.XTick=[datenum('01011994','ddmmyyyy'), datenum('01011995','ddmmyyyy'), datenum('01011996','ddmmyyyy'),...
    datenum('01011997','ddmmyyyy'), datenum('01011998','ddmmyyyy'), datenum('01011999','ddmmyyyy'),...
    datenum('01012000','ddmmyyyy'),  datenum('01012001','ddmmyyyy')]
h.Title.String = ('Taboo Search');
h.YLabel.String = 'p(t)';
h.XLabel.String = 't';
line([tc tc], [0 10000],'Color','b','LineWidth',1)

datetick( 'x', 'yyyy','keeplimits')
str = {['A = '  num2str(round(BS(5),3),'%.3f')]...
    ['B = ', num2str(round(BS(6),3),'%.3f')] ...
    ['C = ', num2str(round(BS(7),3),'%.3f')]...
    ['t_{c} = ', datestr(BS(8),'dd.mm.yy')] ...
    ['z = ', num2str(round(BS(9),3),'%.3f')] ...
    ['\omega = ' num2str(round(BS(10),3),'%.3f')]...
    ['\Phi = ', num2str(round(BS(11),3),'%.3f')]...
    ['R^{2} = ', num2str(round((BS(4)),3),'%.3f')]};


annotation('textbox', [0.15, 0.452,0.1,0.1],...
    'String', str,'interpreter','tex', 'FitBoxToText', 'on', 'BackgroundColor', 'w', 'Margin',6);

legend('NASDAQ continuation','estimated time series NASDAQ', 'fitted values','predicted crash date', 'Location','southwest');

%% Subplot

%subplot(2,1,1)
%text(0.02,0.98,'(a)','Units', 'Normalized', 'VerticalAlignment', 'Top')

[ax4,h3] = suplabel(['Best fits for the period starting in ', datestr(BS(1),'mmm.dd.yy')]  ,'t')
set(h3,'FontSize',12)




%mtit(['Best Fit for starting date' ,datestr(BS(1))],'fontsize',10)%,'xoff',-.1,'yoff',.025);
%set(p.th,'edgecolor',.5*[1 1 1]);

%% use tiks
filename = [ 'Plots.00_' num2str(o) '.tikz' ];

matlab2tikz(filename, 'height', '\figureheight', 'width', '\figurewidth','showInfo', false)
end