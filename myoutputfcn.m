% A demo function demonstrating how to use an Output Function for the
% Genetic Algorithm command GA.  In this demo, an output function is used
% to store the population at each generation and plot it after the genetic
% algorithm has finished processing.
% [POPULATIONS,M] = RUNGA
% POPULATIONS is an (mx1) cell array in which m indicates the generation
% number, and each element of the cell array is the population matrix
% corresponding to the generation.
% M is a movie file containing a scatter plot animation of the populations
% over time.  

function [populations,M] = runga
 
% Preallocate the final populations matrix
populations = cell(100,1);



% Set the options, including the output function, using GAOPTIMSET
options = gaoptimset;
% Modify options setting
options = gaoptimset(options,'PopulationSize', 50, 'Display','iter',...
     'OutputFcns',@output2, 'SelectionFcn', @parents)
 

% Note: in order to specify the max number of iterations, use the GENERATIONS property in GAOPTIMSET.
% For example, the following will stop the GA command after 1 generation:
% options = gaoptimset('OutputFcns',@outfun,'display','iter','Generations',1);

% Call the genetic algorithm

[x,fval,exitflag,output,population,score] = ...
ga(@(x)my_fun(x,y,t),4,[],[],[],[],[],[],[],[],options);


    % This is the output function definition.
    function [state, options,optchanged] = myoutputfcn(options,state,flag)
     optchanged = false;    %no options have been changed
     switch flag
         case 'init'
            fprintf('\n');
            disp('Starting the genetic algorithm...');
         case 'iter'
            % Here is where the current population at each iteration is
            % appended to the final populations cell array.
            currentGeneration = state.Generation;
            currentPopulationMatrix = state.Population;
            curPopScores = state.Score
            populations{currentGeneration} = currentPopulationMatrix;
         case 'done'
            disp('Performing final task...');
         otherwise
     end
    end
 
    % Postprocessing: truncate the extra generations from preallocation
    numGenerations = output.generations;
    populations = populations(1:numGenerations);

    %display final results from the genetic algorithm
    fprintf('\n');
    disp(['final x: ' num2str(x)]);
    disp(['fval: ' num2str(fval)]);
    disp(['exitflag: ' num2str(exitflag)]);

    % Animate the populations.  The red X indicates the final x.
    figure;
    for index = 1:length(populations)
        clf;
        pop = populations{index};
        scatter(pop(:,1),pop(:,2),10);
        hold on;
        plot(x(1),x(2),'rx','MarkerSize',20);
        axis([-1 3 -1 3]);
        M(index) = getframe;
    end
% movie(M,1);

% NOTES: The final variables of interest are:
% populations: a cell array of the population at each generation
% x: the final result of the genetic algorithm
% fval: the value of the fitness function at the final result

end