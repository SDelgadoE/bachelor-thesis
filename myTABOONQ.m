function [SS, RSquared, A, B, C, tc, phi, w, z, yfit] = myTABOONQ(Start, End)
dbstop if error 
warning('off','MATLAB:rankDeficientMatrix');
%% Load nessecary data
load('NQ.mat')
%NQ.prices = log(NQ.prices);
%% Create a new subperiod of the time series

LS = find(NQ.dates==Start);
LE = find(NQ.dates==End);
ts = zeros;
ys = zeros;

ts(1:(LE-LS+1),1) = NQ.dates(LS:LE);
ys(1:(LE-LS+1),1) = NQ.prices(LS:LE);
%% Parameters to be estimated 
% x(1) = tc  ,tc>t(end)
% x(2) = z  ,0<z<1
% x(3) = w  ,5<w<15
% x(4) = phi  ,-2pi<phi<2pi
%% Generate the first population within the parameters
% The initial poulation should be 200 observations large and we keep the best 10 observations 
IP = zeros(200,5);

LB = [ts(end), 0, 5, -2*pi];   % Lower bound
UB = [ts(end)+1200, 1, 15, 2*pi];  % Upper bound

span = UB - LB;
IP(1:end,2:end) = [repmat(LB,200,1) + repmat(span,200,1) .* rand(200,4)];
%% Calculate the initial population scores
for p = 1:200
    x(1)=IP(p,2);
    x(2)=IP(p,3);
    x(3)=IP(p,4);
    x(4)=IP(p,5);
    
    %if x(1) > ts(end) % correct the dates and replace the price beyond the window to last valu of ys
     %   ND = floor(x(1))-ts(end);% find the difference in t. days
      % NewEnd = End + ND; % new ending date
      %[~,I] = min(abs(NQ.dates-NewEnd));%closest day to our new ending date
    % NewEnd2 = NQ.dates(I);
    
    %LS = find(NQ.dates==Start);
    %LE = find(NQ.dates==NewEnd2);
    %t = zeros;
    %y = zeros;
    
    %t(1:(LE-LS+1),1) = NQ.dates(LS:LE);
    %y = ys;
    %y(length(y)+1:length(t))= mean(ys); % replace the dates for which we have no info with some value
    
    %elseif x(1) < ts(end)
    t = ts;
    y = ys;
    %end
    [SS] = myfitnessfcn(x, y, t);
    IP(p,1) = SS;
end


IP((find(imag(IP(:,1)))),:) = [];
IP((find(IP(:,1)==0)),:) = [];

sortedIP = sortrows(IP, 1); % sorted according to their fitness score
%% Save the elite list
Elite = sortedIP(1:10, 1:5);
%% Start History
% first solution to be explored is the best in the elite list
History = Elite(1,:);
%% Create the neighborhood (changes in each iteration)
% Iteration = k
for k = 0:10000
    
    N = zeros(16, 4);
    
    N(:,2) = History(1+k,2);
    N(:,3) = History(1+k,3);
    N(:,4) = History(1+k,4);
    N(:,5) = History(1+k,5);
    
    for a = 1:4 %r = (b-a).*rand(N,1) + a.
        N(a,2) = ((ts(end)+1200)-(t(end)).*rand(1) + (t(end)));
        N(a+4,3) = rand(1);
        N(a+8,4) = (15-5).*rand(1) + 5;
        N(a+12,5) = (2*pi-(-2*pi)).*rand(1) + (-2*pi);
    end
    %% Calculate and save the scores of the neighborhood
    i = 1;
    while i<=16
        x(1)=N(i,2);
        x(2)=N(i,3);
        x(3)=N(i,4);
        x(4)=N(i,5);
        %%% correct the dates and replace the price beyond the window
        
        %if x(1) > ts(end)
         %  ND = floor(x(1))-ts(end);
         % NewEnd = End + ND;
         %[~,I] = min(abs(NQ.dates-NewEnd));
        %NewEnd2 = NQ.dates(I);
        
         %LS = find(NQ.dates==Start);
       % LE = find(NQ.dates==NewEnd2);
        
        %t = zeros;
        %y = zeros;
        
       % t(1:(LE-LS+1),1) = NQ.dates(LS:LE);
        %y = ys;
        %y(length(y)+1:length(t))= mean(ys);
        
        %elseif x(1) < ts(end)
        t = ts;
        y = ys;
        %end
        [SS] = myfitnessfcn(x, y, t);
        if imag(SS) == 0;
            N(i,1) = SS;
            i = i + 1;
        elseif imag(SS) ~= 0 ;
            i = i;
            if i>=1 && i<=4 ;
                N(i,2)= (t(end)+1000-(t(end)).*rand(1) + (t(end)));
            end
        elseif i>=5 && i<=8 ;
            N(i,3) = rand(1);
        elseif i>=9 && i<=12 ;
            N(i,4) = (15-5).*rand(1) + 5;
        elseif i>=13 && i<=16 ;
            N(i,5) = (2*pi-(-2*pi)).*rand(1) + (-2*pi);
        end
    end
    % delete any NaN values in the score
    %nd = find(isnan(N));
    %N(nd,:) = [];
    
    %% Find the best move( Which is one with a negative move value closest to zero) and save it
    % We have to check if the move doesn't fall within the tabu criteria
    % Tabu criteria #1 the move is not to a solution we have already used
    % Tabu criteria #2 the moves is tabu if it results in a deterioration of
    % the score greater than some specified value
    % To check for the tabu criteria we create a tabu list of lenght L which changes at each iteration and consists of the last parameters used
    L = 15;
    if size(History,1) >= L;
        TL(1:L,1:5) = History(((size(History,1)-L+1):size(History,1)),1:5);
    elseif size(History,1) < L;
        TL = History(:,1:5);
    end
    % Delete any in entry in N that is the same as one in our Tabu List #1
    N((find(ismember(N,TL, 'rows'))),:) = [];
    
    % Calculate the move values
    MV = N(:,1)-History(1+k,1);
    MV(isnan(MV)) = [];
    %% find the best move value
    %% if no negative move malue choose the smallest positive value if its within a certain range
    % decide the how big the threshold value should be according to the
    % number of iteratons = th
    if k<50
        th = 0.4;
    elseif k>=50 && k<100
        th = 0.1;
    elseif k>=100 && k<150
        th =0.01;
    elseif k>=150 && k<200
        th = 0.001;
    elseif k>=200
        th = 0;
        
    end
    if any(MV<0) == 1
        BM = find((MV==max(MV(MV<0))),1); % find the closest to zero negative value
        History(k+2,:) = N(BM, :);
        
    elseif (all(MV>=0)) == 1; 
        potentialBM = find((MV==min(MV(MV>0))),1); % find the smallest positive move value
        if MV(potentialBM) < th % accept it if its bellow a threshhold
            History(k+2,:) = N(potentialBM,:);
            %% Here the search is directed to another elite solution
        elseif MV(potentialBM) > th
            pbm = setdiff(Elite,History,'rows');
            if isempty(pbm) == 1
                %fprintf('Elite solutions exahusted')
                %disp(k)
                break
            end
            History(k+2,:) = pbm(1,:);
            %fprintf('New Elite solution used')
            %disp(k)
        end
    end
    
    SortedHistory = sortrows(History,1);
end
%% Show the best solution
t = ts;
y = ys;
BestSolution = SortedHistory(1,2:5);
x(1)=BestSolution(1,1); 
x(2)=BestSolution(1,2); 
x(3)=BestSolution(1,3); 
x(4)=BestSolution(1,4); 
[SS, OLS] = myfitnessfcn(x, y, t);
%% Create an alternate t and y so we can show the full value of yfit
if x(1)>ts(end);
    ND = floor(x(1))-ts(end);
    NewEnd = End + ND;
    [~,I] = min(abs(NQ.dates-NewEnd));
    NewEnd2 = NQ.dates(I);
    
    LS = find(NQ.dates==Start);
    LE = find(NQ.dates==NewEnd2);
    
    t2 = zeros;
    y2 = zeros;
    
    t2(1:(LE-LS+1),1) = NQ.dates(LS:LE);
    y2(1:(LE-LS+1),1) = NQ.prices(LS:LE);
    
elseif x(1)<ts(end);
    t2 = ts;
    y2 = ys;
end

%% Calculate yfit
ft = (x(1)-t2).^x(2);
gt = ((x(1)-t2).^x(2)).*cos(x(3).*log(x(1)-t2)+x(4));

yfit = OLS(1) + OLS(2).*ft + OLS(3).*gt; 
% delete complex values in yfit and correct t2 accordingly
d = find(imag(yfit(:,1)),1);
yfit(d:end,:) = [];
t2(d:end,:) = [];
y2(d:end,:) = [];
%% Plot
%figure
%plot(t2,y2)
%datetick( 'x', 'dd-mm-yyyy' )
%hold on
%plot(t2, yfit)
%title('Taboo1')

Ybar = mean(y2);
TotalSS = sum((y2-Ybar).^2);
ResidualSS = sum((y2-yfit).^2);
RSquared = 1-(ResidualSS/TotalSS);
%DatePredicted = datestr(x(1));
A =OLS(1);
B = OLS(2);
C = OLS(3);
tc = x(1);
phi = x(4);
w = x(3);
z = x(2);
%% Levenber-Marquardt nonlinear least squares algorithm
%lb = [(t(end)-10); 0; 5; -2*pi];   % Lower bound
%ub = [Inf; 1; 15; 2*pi];  % Upper bound
lb = [-Inf; -Inf; -Inf; -Inf];   % Lower bound
ub = [Inf; Inf; Inf; Inf];  % Upper bound
%xold = x;
% use the same dates and prices as the taboo search
t = ts;
y = ys;
options = optimoptions(@lsqnonlin, 'TolFun',1e-8, 'Algorithm','levenberg-marquardt','Diagnostics','off','FinDiffType','central','display','off');
[LMx] = lsqnonlin(@(x)myfitnessfcn(x, y ,t),[x(1),x(2), x(3), x(4)],lb,ub,options);
x = LMx;
[SSLMx, OLS] = myfitnessfcn(x, y, t);
% check to see if it found a better soultion and if so calculate all the
% new figures

if SSLMx<SS
    %fprintf('lsqnonlin found a better solution')
    if LMx(1)>t(end) % correct dates and prices
        ND = floor(x(1))-t(end);
        NewEnd = End + ND;
        [~,I] = min(abs(NQ.dates-NewEnd));
        NewEnd2 = NQ.dates(I);
        
        LS = find(NQ.dates==Start);
        LE = find(NQ.dates==NewEnd2);
        
        t2 = zeros;
        y2 = zeros;
        
        t2(1:(LE-LS+1),1) = NQ.dates(LS:LE);
        y2(1:(LE-LS+1),1) = NQ.prices(LS:LE);
       
    elseif LMx(1)<t(end)
        t2 = ts;
        y2 = ys;
    end
    
    ft = (x(1)-t2).^x(2);
    gt = ((x(1)-t2).^x(2)).*cos(x(3).*log(x(1)-t2)+x(4));
    yfit = OLS(1) + OLS(2).*ft + OLS(3).*gt;
    d = find(imag(yfit(:,1)),1);
    yfit(d:end,:) = [];
    t2(d:end,:) = [];
    y2(d:end,:) = [];
  
    %figure;
    %plot(t2,y2);
    %datetick( 'x', 'dd-mm-yyyy' );
    %hold on;
    %plot(t2, yfit);
    %title('Taboo2');
        
    %DatePredicted = datestr(x(1));
    Ybar = mean(y2);
    TotalSS = sum((y2-Ybar).^2);
    ResidualSS = sum((y2-yfit).^2);
    RSquared = 1-(ResidualSS/TotalSS);
    A =OLS(1);
    B = OLS(2);
    C = OLS(3);
    tc = x(1);
    phi = x(4);
    w = x(3);
    z = x(2);
    SS = SSLMx;
elseif SSLMx>SS;
    %fprintf('lsqnonlin did not find a better solution')
elseif SSLMx==SS;
    %fprintf('lsqnonlin found the same solution')
end
end